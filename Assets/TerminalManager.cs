using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerminalManager : MonoBehaviour
{
    public GameObject directoryLine;
    public GameObject responseLine;

    public InputField terminalInput;
    public GameObject userInputLine;
    public ScrollRect sr;
    public GameObject msgList;
    public CharacterController player;
    public GameObject playerCamera;
    Interpreter interpreter;
    RaycastHit terminalhit;
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
        interpreter = GetComponent<Interpreter>();
    }

    string userInput;
    Vector2 msgListSize;
    void ClearInputField()
    {
        terminalInput.text = "";
    }

    void AddDirectoryLine(string userInput)
    {
        //Resisign the command line container
        msgListSize = msgList.GetComponent<RectTransform>().sizeDelta;
        msgList.GetComponent<RectTransform>().sizeDelta = new Vector2(msgListSize.x, msgListSize.y+35.0f);

        //Instantiate the directory line 
        GameObject msg = Instantiate(directoryLine, msgList.transform);

        //Set child index
        msg.transform.SetSiblingIndex(msgList.transform.childCount - 1);

        //Set the text
        msg.GetComponentsInChildren<Text>()[1].text = userInput;
    }

    int AddInterpreterLines(List<string> interpretation)
    {
        for (int i = 0; i< interpretation.Count; i++)
        {
            //Instantiate the response line
            GameObject res = Instantiate(responseLine, msgList.transform);

            res.transform.SetAsLastSibling();

            Vector2 listSize = msgList.GetComponent<RectTransform>().sizeDelta;
            msgList.GetComponent<RectTransform>().sizeDelta = new Vector2(listSize.x, listSize.y+35.0f);

            res.GetComponentInChildren<Text>().text = interpretation[i];
        }
        return interpretation.Count;
    }

    void ScrollToBottom(int lines)
    {
        if (lines > 4)
        {
            sr.velocity = new Vector2(0, 450);
        }
        else
        {
            sr.verticalNormalizedPosition = 0;
        }
    }
    private void OnGUI()
    {
        //Freeze the player while using the terminal 
        if (terminalInput.isFocused)
        {
            player.enabled = false;
        }
        else
        {
            player.enabled = true;
        }

        if (terminalInput.isFocused && terminalInput.text != "" & Input.GetKeyDown(KeyCode.Return))
        {
            //Store user text input
            userInput = terminalInput.text;

            //Clear input field
            ClearInputField();

            AddDirectoryLine(userInput);

            //Add interpretation lines
            int lines = AddInterpreterLines(interpreter.Interpret(userInput));

            //Sctroll to the bottom of the output.
            ScrollToBottom(lines);

            //Move the user input line to the bottom
            userInputLine.transform.SetAsLastSibling();

            //Refocus
            terminalInput.ActivateInputField();
            terminalInput.Select();

        }
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
            if (Physics.Raycast(ray, out terminalhit))
            {
                if (terminalhit.collider.name == "terminal")
                {
                    terminalInput.Select();
                    terminalInput.ActivateInputField();
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
