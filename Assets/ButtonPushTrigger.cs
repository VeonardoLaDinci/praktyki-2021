using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

public class ButtonPushTrigger : MonoBehaviour
{
    RaycastHit hit;
    string terminalId;
    GameObject terminal;
    Animation anim;
    public void Start()
    {
        terminal = GameObject.FindGameObjectWithTag("Terminal");
        terminalId = terminal.GetComponent<Interpreter>().terminalId;
        
    }
    
    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            
            Ray ray = new Ray(transform.position, transform.forward);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.isTrigger)
                {
                    string name = hit.collider.tag;
                    //string directory = Application.streamingAssetsPath + "/" + terminalId + "/";
                    StreamReader file;
                    //if (File.Exists(Path.Combine(Application.streamingAssetsPath, terminalId + "/" + name+".txt")))
                    //{
                    file = new StreamReader(Path.Combine(Application.streamingAssetsPath, terminalId + "/" + name + ".script"));
                    //}
                    terminal.GetComponent<Interpreter>().Interpret(file.ReadLine());
                    anim = hit.collider.GetComponentInChildren<Animation>();
                    anim.Play("Armature_buttonaction");
                }
            }
        }
    }

}
