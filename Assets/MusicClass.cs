using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicClass : MonoBehaviour
{
    private AudioSource _audioSource;
    static bool togglePlay = true;
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        _audioSource = GetComponent<AudioSource>();
    }
    public void TogglePlay()
    {
        if (togglePlay)
        {
            togglePlay = false;
        }
        else
        {
            togglePlay = true;
        }
    }
    public void PlayMusic()
    {
        if (_audioSource.isPlaying) return;
        _audioSource.Play();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }
    private void Update()
    {
        if (togglePlay)
        {
            PlayMusic();
        }
        else
        {
            StopMusic();
        }
    }

}
