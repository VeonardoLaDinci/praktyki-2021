using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScriptt : MonoBehaviour
{
    public Vector3 movePosition;
    public Vector3 startPosition;
    public Vector3 currentPositon;
    float frames = 0;
    float maxFrames = 90;
    void Start()
    {
        startPosition = transform.position;
        movePosition = startPosition;
    }

    void FixedUpdate()
    {
        //if (frames > maxFrames) return;
        float t = frames / maxFrames;
        if (movePosition != startPosition)
        {
            //GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(startPosition, movePosition, t));
            GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(startPosition, movePosition, (float)t));
            frames++;

        }



        if (movePosition == transform.position)
        {
            startPosition = movePosition;
        }
        currentPositon = transform.position;

    }

    void OnCollisionEnter(Collision collision)
    {
        startPosition = transform.position;
        movePosition = transform.position;
    }
}
