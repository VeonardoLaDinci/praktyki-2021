using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public Vector3 a;
    public Vector3 b;
    public Vector3 movePosition;
    public Vector3 startPosition;
    public Vector3 currentPositon;
    public Transform Player;
    public float frames = 0;
    float maxFrames = 90;
    void Start()
    {
        startPosition = transform.position;
        movePosition = startPosition;
    }   
    void FixedUpdate()
    {
        //if (frames > maxFrames) return;
        float t = frames / maxFrames;
        if (movePosition != startPosition)
        {
            //GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(startPosition, movePosition, t));
            //GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(startPosition, movePosition, (float) t));
            transform.position = Vector3.Lerp(startPosition, movePosition, (float) t);
            frames++;
        }
        if (movePosition == transform.position)
        {
            startPosition = movePosition;
        }
        currentPositon = transform.position;
    }
    void OnCollisionEnter(Collision collision)
    {
        startPosition = transform.position;
        movePosition = transform.position;
    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.transform == Player)
        {
            Player.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == Player)
        {
            Player.transform.parent = null;
        }
    }
}
