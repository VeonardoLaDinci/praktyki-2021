using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading;
public class Interpreter : MonoBehaviour
{
    public string terminalId = "terminal_test";
    public GameObject box;
    Vector3 rayDirection;
    string edir;
    List<string> response = new List<string>();
    Dictionary<string, string> colors = new Dictionary<string, string>()
    {
        {"dark blue", "#475F94"},
        {"light blue", "#107AB0"},
        {"gold", "#FDDC5C"},
        {"red", "#FA4224"},
        {"rose", "#FDC1C5"},
        {"grapefruit", "#FD5956"},
        {"hotpink", "#FF69B4"}
    };

    public List<string> Interpret(string userInput)
    {
        response.Clear();
        string[] args = userInput.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);

        switch (args[0])
        {
            case "help":
                ListEntry("help", "lists commands that you can use in the terminal");
                ListEntry("activate", "used to operate a movable object");
                ListEntry("echo", "returns a string; adds content to text files");
                ListEntry("open", "opens an environmental object with the specified id");
                ListEntry("ls", "used to show files stored in the terminal's storage");
                ListEntry("touch", "creates a file");
                ListEntry("rm", "removes a file");
                ListEntry("ascii", "returns the contents of a specified file");
                ListEntry("reset", "resets the current level");
                return response;
            case "open":
                if (args.Length > 1) 
                {
                    if (GameObject.FindGameObjectsWithTag(args[1]).Length>0)
                    {
                        foreach (GameObject door in GameObject.FindGameObjectsWithTag(args[1]))
                        {
                            if (door.GetComponent<Rigidbody>())
                            {
                                door.GetComponent<Rigidbody>().isKinematic = false;
                                response.Add("opened the door id " + ColorString(args[1], "red"));
                            }                      
                        }
                    }
                    else
                    {
                        response.Add("no door of that id found");
                    }
                }
                else
                {
                    response.Add("please provide an id");
                }
                return response;
            case "move":
                //int m = 0;
                float x, y, z;
                if (args.Length > 4)
                {
                    GameObject mobject = GameObject.Find(args[1]);
                    if (mobject)
                    {
                        if (mobject.GetComponent<Rigidbody>())
                        {
                            mobject.GetComponent<MoveObject>().frames = 0;
                            x = (float)System.Convert.ToDouble(args[2]);
                            y = (float)System.Convert.ToDouble(args[3]);
                            z = (float)System.Convert.ToDouble(args[4]);
                            TeleportObject(mobject, x, y, z);
                            response.Add("moved an object with id " + ColorString(args[1], "red"));
                        }
                    }
                    else
                    {
                        response.Add("no object found");
                    }
                }
                else
                {
                    response.Add("not enough arguments");
                }
                return response;
            case "activate":
                if (args.Length > 1)
                {
                    GameObject[] mobjects = GameObject.FindGameObjectsWithTag(args[1]);
                    foreach (GameObject mobject in mobjects)
                    {
                        if (mobject)
                        {

                            Vector3 a = mobject.GetComponent<MoveObject>().a;
                            Vector3 b = mobject.GetComponent<MoveObject>().b;
                            if (mobject.transform.position == b)
                            {
                                mobject.GetComponent<MoveObject>().frames = 0;
                                TeleportObject(mobject, a.x, a.y, a.z);
                            }
                            else if (mobject.transform.position == a)
                            {
                                mobject.GetComponent<MoveObject>().frames = 0;
                                TeleportObject(mobject, b.x, b.y, b.z);
                            }

                            response.Add("moved an object with id " + ColorString(args[1], "red"));
                        }
                        else
                        {
                            response.Add("no object found");
                        }
                    }
                }
                else
                {
                    response.Add("not enough arguments");
                }
                return response;
            case "ascii":
                string pom = "";
                if (args.Length>1) { pom = args[1]; };
                LoadTitle(pom,colors["gold"]);
                return response;
            case "ls":
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Application.streamingAssetsPath, terminalId + "/"));
                FileInfo[] info = dir.GetFiles("*.*");
                foreach (FileInfo f in info)
                {
                    if (!f.Name.Contains("meta"))
                    {
                        response.Add(ColorString(f.Name,colors["grapefruit"]));
                    }
                }
                return response;
            case "echo":
                //DirectoryInfo echodir = new DirectoryInfo(Path.Combine(Application.streamingAssetsPath, terminalId + "/"));
                edir = Application.streamingAssetsPath + "/" + terminalId + "/";
                string stringarg = "";
                
                if (userInput.Split('"').Length > 1)
                {
                    stringarg = userInput.Split('"')[1];
                }
                //stringarg = ColorString(stringarg, colors["hotpink"]);
                if (userInput.Split('"').Length > 2)
                {
                    string filename = userInput.Split('"')[2].Replace(" ", ""); ;
                    if (File.Exists(edir + filename))
                    {
                        File.AppendAllText(edir + filename, stringarg);
                        response.Add(ColorString(stringarg, colors["hotpink"]) + " appended to " + filename);
                        return response;
                    }
                    else
                    {
                        response.Add("No file"+ filename +" exists");
                    }
                    
                }
                response.Add(ColorString(stringarg, colors["hotpink"]));
                return response;
            case "rm":
                //DirectoryInfo echodir = new DirectoryInfo(Path.Combine(Application.streamingAssetsPath, terminalId + "/"));
                edir = Application.streamingAssetsPath + "/" + terminalId + "/";
                if (args.Length > 1)
                {
                    if(File.Exists(edir + args[1]))
                    {
                        File.Delete(edir + args[1]);
                        response.Add("Deleted file " + ColorString(args[1], "light blue"));
                    }
                    else
                    {
                        response.Add("No such file exists");
                    }
                }
                else
                {
                    response.Add("No file name provided");
                }
                
                return response;
            case "touch":
                edir = Application.streamingAssetsPath +"/"+ terminalId + "/";
                if (args.Length > 1)
                {
                    File.Create(edir + args[1]);
                    response.Add("file " + args[1] + " created");
                }
                else
                {
                    response.Add("provide a filename");
                }
                return response;
            case "reset":
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
                return response;

            default:
                response.Add("I don't know this command. Please refer to the manual using \"help\" ");
                return response;
        }
            
    }

    public string ColorString(string s, string color)
    {
        string leftTag = "<color=" + color + ">";
        string rightTag = "</color>";
        return leftTag+s + rightTag;
    }

    void ListEntry(string a, string b)
    {
        response.Add(ColorString(a, colors["red"])+": "+ColorString(b,colors["rose"]));
    }

    void LoadTitle(string path="nofile.txt", string color="red")
    {
        StreamReader file;
        if (File.Exists(Path.Combine(Application.streamingAssetsPath, terminalId + "/" + path)))
        {
            file = new StreamReader(Path.Combine(Application.streamingAssetsPath, terminalId +"/"+ path));
            while (!file.EndOfStream)
            {
                response.Add(ColorString(file.ReadLine(), color));
            }
        }
        else
        {
            file = new StreamReader(Path.Combine(Application.streamingAssetsPath, terminalId +"/"+"nofile.txt"));
            while (!file.EndOfStream)
            {
                response.Add(ColorString(file.ReadLine(), color));
            }
        }
        
        file.Close();

    }
    //public int interpolationFramesCount = 45; // Number of frames to completely interpolate between the 2 positions
    //int elapsedFrames = 0;

    void TeleportObject(GameObject tobject, float x=0, float y=0, float z=0)
    {
        tobject.GetComponent<MoveObject>().startPosition = tobject.transform.position;
        tobject.GetComponent<MoveObject>().movePosition = new Vector3(x, y, z);
    }
}
