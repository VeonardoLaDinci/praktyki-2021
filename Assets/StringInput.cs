using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StringInput : MonoBehaviour
{
    public string theInput = "";
    public GameObject[] display = new GameObject[10];
    public InputField inputFieldName;
    public GameObject inputField;
    public GameObject outputField;

    public void EnterText()
    {
        for(int i = 9; i>0; i--) {
            display[i].GetComponent<Text>().text = display[i-1].GetComponent<Text>().text;
        }
        inputFieldName.ActivateInputField();
        theInput = "\n" + inputField.GetComponent<Text>().text;
        display[0].GetComponent<Text>().text = theInput;
        inputFieldName.Select();
        inputFieldName.text = "";

    }

    public void Start()
    {
        //inputFieldName.ActivateInputField(); 
    }
    public void Update()
    {

        if (Input.GetButtonDown("Enter")&& inputFieldName.text != "")
        {
            EnterText();
        }
    }
}
