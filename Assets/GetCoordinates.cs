using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UI;

public class GetCoordinates : MonoBehaviour
{
    public CharacterController p;
    public GameObject coordinates;
    public GameObject gobject;
    public GameObject playerCamera;
    RaycastHit hit;
    GameObject panel;
    private Camera cameraFreeWalk;
    public float zoomSpeed = 20f;
    public float minZoomFOV = 10f;
    public float maxZoomFOV = 0f;
    private void Start()
    {
        panel = GameObject.FindGameObjectsWithTag("Panel")[0];
        panel.SetActive(false);
    }

    public void ZoomIn()
    {
        cameraFreeWalk.fieldOfView -= zoomSpeed / 8;
        if (cameraFreeWalk.fieldOfView < minZoomFOV)
        {
            cameraFreeWalk.fieldOfView = minZoomFOV;
        }
    }
    public void ZoomOut()
    {
        cameraFreeWalk.fieldOfView -= zoomSpeed / 8;
        if (cameraFreeWalk.fieldOfView < minZoomFOV)
        {
            cameraFreeWalk.fieldOfView = minZoomFOV;
        }
    }
    void Update()
    {
        coordinates.GetComponent<UnityEngine.UI.Text>().text = System.Convert.ToString(p.transform.position);

        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            gobject.GetComponent<UnityEngine.UI.Text>().text = hit.collider.tag;
        }
        else
        {
            gobject.GetComponent<UnityEngine.UI.Text>().text = "none";
        }
        
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            panel.SetActive(true);
        }
        else if (Input.GetKeyUp(KeyCode.Tab))
        {
            panel.SetActive(false);
        }

    }

}
